import React from 'react'

// For initializing react context and holding the data
const UserContext = React.createContext()

// For providing data/state to all components
export const UserProvider = UserContext.Provider

export default UserContext