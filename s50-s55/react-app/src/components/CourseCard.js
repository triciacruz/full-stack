import { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';

export default function CourseCard({course}){
	
	// You can destructure props twice to be able to access their properties deirectly without using dot (.) notaion in the JSX code
	const {_id, name, description, price} = course

	// // Declaration of the 'count' state
	// // To declare a state, you must have a getter and setter.
	// // The getter is responsible for getting the value of the state which is within the useState() function
	// // The setter is responsible for setting a new value to the state.
	// // Note: You cannot change the value of the state without using the setter.
 //  	const [enrollees, setEnrollees] = useState(0);
 //  	const [seats, setSeats] = useState(10);
 //  	const [isOpen, setIsOpen] = useState(true);
	// // MINI ACTIVITY (30 mins):
	
	// 	- Create a 'seats' state which will represent the number of available seats in the course.

	// 	- Put the value of the seats state in the card itself.

	// 	- Everytime the 'enroll' button is click, the seats state  must go down in value by 1.

	// 	- Stretch Goal: When the value of the seat state is 0, disable the 'enroll' button.
	
	// /*
	// function enroll() {
	// 	if (seats > 0) {
	// 	    setSeats(seats - 1);
	// 	    setEnrollees(enrollees + 1);
	// 	}
	// }
	// */

	// // SIR EARL ANSWER
	// function enroll() {
	// 	setEnrollees(enrollees + 1)
	// 	setSeats(seats - 1)
	// }

	// // useEffect has 2 arguments, an arrow unction and an array. The array will hold the specific state that you want to observe changes for. If the is a change in the state nside the array, then the arrow function will run. You can leave the array empty and this will not observe any state.
	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [seats])

	return(
		<Card className="my-3">
	      <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>Php {price}</Card.Text>

	        <Link className="btn btn-primary" to={`/courses/${_id}/view`}>View Course</Link>
	      </Card.Body>
	    </Card>
	)
}	

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


// solution 2
/*
import {Card, Button} from 'react-bootstrap';

export default function CourseCard({course}){
	return(
		<Card>
	      <Card.Body>
	        <Card.Title>{course.name}</Card.Title>
	        
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{course.description}</Card.Text>
	        
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>Php {course.price}</Card.Text>

	        <Button variant="primary">Enroll</Button>

	      </Card.Body>
	    </Card>
	)
}
*/

//solution 3
/*
// import {Button, Row, Col, Card} from 'react-bootstrap'; //FROM ACTIVITY

export default function CourseCard(props){
	return(
	//	<Row className="my-3">
		//	<Col xs={12} md={4}>
				<Card>
			      <Card.Body>
			        <Card.Title>{props.course.name}</Card.Title>
			        
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{props.course.description}</Card.Text>
			        
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>Php {props.course.price}</Card.Text>

			        <Button variant="primary">Enroll</Button>

			      </Card.Body>
			    </Card>
	//		</Col>
	//	</Row>
	)
}
*/


//========================
//SIR EARL ACTIVITY ANSWER 
/*
import {Card, Button} from 'react-bootstrap';

export default function CourseCard(props){
	return(
	<Card>
		<Card.Body>
			<Card.Title>Sample Course</Card.Title>
			    <Card.Subtitle>Description:</Card.Subtitle>
			    <Card.Text>
			          This is a sample course
			    </Card.Text>
			    <Card.Subtitle>Price:</Card.Subtitle>
			    <Card.Text>
			       Php 40,000
			</Card.Text>
			<Button variant="primary">Enroll</Button>
		</Card.Body>
	</Card>
	)
}

*/