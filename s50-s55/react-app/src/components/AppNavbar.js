// To use .. from the react-bootstrap package
import {Fragment, useState, useContext} from 'react'
import {Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar(){

	/* Gets the user email after the user has logged in (connected to login.js) */
	const {user} = useContext(UserContext)

	return(
		<Navbar bg="light" expand="lg">
        	<Container fluid>
         		<Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
         		<Navbar.Toggle aria-controls="basic-navbar-nav"/>
         		<Navbar.Collapse id="basic-navbar-nav">
         			<Nav className="mr-auto">
	         			<Nav.Link as={NavLink} to="/">Home</Nav.Link>
	         			<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

	         			{ (user.id !== null) ?
	         				<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
	         				:
	         				<Fragment>
			         			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			         			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		         			</Fragment>
	         			}
	         			
         			</Nav>
         		</Navbar.Collapse>
        	</Container>
      	</Navbar>
	)
}

{/*If the user has logged in, only show the logout link*/}

{/*If the user has not logged in, show both login and register links*/}