import {useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'


export default function Logout(){
	
	const {unsetUser, setUser} = useContext(UserContext)

	// Clears the localStorage (which will clear he user data)
	unsetUser()

	// Clears the user state from App.js upon the initial loading of the logout component
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
	}, [])

	// Navigates/redirects to the '/login' route
	return(
		<Navigate to="/login"/>
	)
}