import {Fragment} from 'react'
import {Link, useNavigate} from 'react-router-dom'
import {Button} from 'react-bootstrap'

export default function NotFound(){
	const  navigate = useNavigate()
	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p>The page you are looking for does not exist or an other error occured.</p>
			<p>Return to <Link as={Link} to="/">Home</Link> page.</p>
			
			<Button variant="primary" onClick={() => navigate(-1)}>Go back to previous page</Button>
		</Fragment>
	)
}