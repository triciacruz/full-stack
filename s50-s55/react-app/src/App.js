// Importables
import {Fragment, useState} from 'react';
import AppNavbar from './components/AppNavbar';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import CourseView from './components/CourseView.js';
import {Container} from 'react-bootstrap';
import './App.css';
import {UserProvider} from './UserContext.js';


// Component function
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  // The componenct function returns JSX syntax that serves as the UI of the component.
  // Note: JSX syntax may look like HTML but it is acually Javascript that is formatted to look like HTML and is not actually HTML. The benefit of JSX is the ability to easily integrate Javascript with HTML syntax.

  return (

    // When rendering multiple components, they must always be enclosed in a parent componen/element.

  <UserProvider value={{user, setUser, unsetUser}}>

    <Router> {/* The  'Router' initializes the dynamic routing to be involved. */}
      <AppNavbar/>
      <Container>
          <Routes> {/* The 'Routes' initialized the set of specific routes to be used. */}
            {/*The 'Route is the specific endpoint which will render a specific component.*/}
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/courses/:courseId/view" element={<CourseView/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
          
          {/*
           - Create a route which will handle any route that is not defined above. 
           - That route will load a 'NotFound' page will have the following elements:
            a. h1 (Title)
            b. p (content)
            c. Link (from react-router) which will go back to the previous page.
          */}
           <Route path="*" element={<NotFound/>}/>
          </Routes>
      </Container>
    </Router>
  </UserProvider>
 
  );
}

// Exporting the component function
export default App;

