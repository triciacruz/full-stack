// The 'document' represetns the whole HTML page, and the query selector targets a specific element based on its ID, class, or tag name

// id = #
//console.log(document.querySelector("#txt-first-name"))

/*
// class = .
console.log(document.querySelector(".txt-first-name"))
*/

// These are the specific selectors you can use for targeting specific target method (ID, class, tag name)
// Note: These aren't common to use anymore as compared to
/*
document.getElementById("txt-first-name")
document.getElementByClassName("span-full-name")
document.getElementByTagName("input")
*/

// [SECTION] Event Listener
/*
const txt_first_name = document.querySelector("#txt-first-name")
let span_full_name = document.querySelector(".span-full-name")

//The 'addEventListener' function listens for an event in a specific HTML tag. Anytime an event is triggered, it will run the function in its 2nd argument.
txt_first_name.addEventListener ('keyup', (event) => {
	span_full_name.innerHTML = txt_first_name.value
})


txt_first_name.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
	console.log(event.target)
	console.log(event.target.value)
})
*/


//DO NOT REPEAT YOURSELF (DRY PRINCIPLE)
// ACTIVITY
const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
let span_full_name = document.querySelector(".span-full-name")

txt_first_name.addEventListener('keyup', (event) => {
  span_full_name.innerHTML = `${txt_first_name.value} ${txt_last_name.value}`
})

//REPEATED 
txt_last_name.addEventListener('keyup', (event) => {
  span_full_name.innerHTML = `${txt_first_name.value} ${txt_last_name.value}`

  console.log(event.target)
  console.log(event.target.value)
})


/*
//Sir Earl Answer
const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
let span_full_name = document.querySelector(".span-full-name")

const updateFullName = () => {
	let first_name = txt_first_name.value
	let last_name = txt_last_name.value

	span_full_name.innerHTML = `${first_name} ${last_name}`
}

txt_first_name.addEventListener('keyup', updateFullName)
txt_last_name.addEventListener('keyup', updateFullName)
*/


//STRETCH GOALS
//NEW ACTIVITY CODE USING FUNCTION
const txt_age = document.querySelector("#txt-age")
const txt_address = document.querySelector("#txt-address")
let span_greeting = document.querySelector(".span-greeting")

function updateGreeting = () => {
  const firstName = txt_first_name.value
  const lastName = txt_last_name.value
  const age = txt_age.value
  const address = txt_address.value
  let greeting = `Hello, I am ${firstName} ${lastName}, ${age} years old. I am from ${address}.`
  span_greeting.innerHTML = greeting
}

txt_first_name.addEventListener('keyup', updateGreeting)
txt_last_name.addEventListener('keyup', updateGreeting)
txt_age.addEventListener('keyup', updateGreeting)
txt_address.addEventListener('keyup', updateGreeting)


